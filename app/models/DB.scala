package models

import play.api.libs.json._
import scala.concurrent.{ExecutionContext, Future}

/**
 * Created by johann on 30/07/14.
 */

trait DB{
  type ID
  def persist(obj:JsValue):Future[Option[JsObject]]
  def getByKeys(keys:ID *):Future[List[JsObject]]
  def getByKey(key:ID)(implicit executor:ExecutionContext):Future[Option[JsObject]] = getByKeys(key).map( _.headOption )
  def dump():Future[List[JsObject]]
}

object ActorDB extends DB{

  import akka.actor._
  import akka.pattern.ask
  import akka.util.Timeout
  import play.api.libs.json._
  import scala.concurrent.ExecutionContext.Implicits.global
  import scala.concurrent._
  import scala.concurrent.duration._

  type ID = Int

  private lazy val system = ActorSystem("mySystem")
  private implicit lazy val timeout = Timeout(10 minutes)

  private case class STORE(value:JsObject)
  private case class BY_ID_REQ(ids:List[ID])
  private object BY_ID_REQ{
    def apply(id:ID):BY_ID_REQ = BY_ID_REQ(List(id))
  }
  private case object DUMP

  private def withID(id:ID,json:JsObject) = json ++ Json.obj("id" -> id)
  val withIDTupled = (withID _).tupled

  private lazy val storageActor:ActorRef = system.actorOf(Props(new Actor{
    import context._
    private def receiving(storage:Map[ID,JsObject]):Receive = {

      case BY_ID_REQ(ids) =>
        sender() ! storage.collect{ case tuple @ (id,_) if ids.contains(id) => withIDTupled(tuple) }.toList

      case STORE(value) =>
        storage.collectFirst{ case (id,tValue) if tValue == value => sender() ! id }.
        getOrElse{
          val key = storage.size
          val newTuple = key -> value
          val updatedStorage = storage + newTuple
          become(receiving(updatedStorage))
          sender() ! key
        }

      case DUMP =>
        sender() ! storage.map( withIDTupled ).toList
    }

    override def receive: Receive = receiving(Map.empty)
  }))

  def persist(obj:JsValue):Future[Option[JsObject]] = obj match{
    case obj:JsObject =>
      (storageActor ? STORE(obj)).mapTo[ID].flatMap{ case id => getByKey(id) }
    //we only know how to store objects...
    case _ => Future.successful(Option.empty)
  }


  def getByKeys(keys:ID *):Future[List[JsObject]] =
    (storageActor ? BY_ID_REQ(keys.toList)).mapTo[List[JsObject]]

  def dump = (storageActor ? DUMP).mapTo[List[JsObject]]

}
