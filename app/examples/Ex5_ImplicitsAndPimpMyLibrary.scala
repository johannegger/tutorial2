package examples

object Ex5_ImplicitsAndPimpMyLibrary {
  import org.joda.time.DateTime
  import Ex1_FunctionsAndVariables._

  //type definition via structural type!
  type Logger = AnyRef{ def log(message:String):Unit }
  //we could have done:
 /*
 trait Logger{
    def log(message:String):Unit
  }
*/

  //define a method that takes a normal parameter and one that is implicit
  //look out for the call-by-name parameter!
  def loggedWithTimestamp(message: => String)(implicit logger:Logger):String = {
    //btw currentTimeMillis is JAVA
    logger.log(s"at ${System.currentTimeMillis()} $message")
    message
  }

  //define us a dummy logger
  val DefaultLogger:Logger = new{
    def log(message:String) = println(message)
  }

  //this: loggedWithTimestamp("no implicit logger") does not compile!

  //call the method with explicit logger
  loggedWithTimestamp("logger was explicitly provided")(DefaultLogger)

  def testLogging() = {
    implicit val myLogger = DefaultLogger
    loggedWithTimestamp(uppercased("hello world"))

  }

  //pimp-my-library
  //adding functionality to existing types -> especially useful for existing java classes
  implicit def timestampPimp(obj:Long) = new{
    def asDate = new DateTime(obj)
  }

  val now = System.currentTimeMillis().asDate
  now.getDayOfMonth

  //actually don't do this because joda already has
  //new DateTime()

}
