package examples

object Ex2_ClassesObjectsAndTraits {

  //creating a simple class
  class DummyMessage{
    val sender = "me"
    def receiver = "you"
    lazy val body = "something very important"
  }
  //create an instance
  val myDummyMessage = new DummyMessage()
  //access fields
  myDummyMessage.sender

  //creating a case class with parameters
  case class Message(
    val sender: String,
    val receiver: String,
    val body: String
  )
  //create an instance
  val myMessage = Message("me","you","something very important")

  //define another constructor via companion object
  // note: explain singletons in scala
  // and the powers of companions -> can read privates of class...
  object Message{
    val EmptyMessage = Message("","","")
    //we could define a second constructor like this:
    //def apply():Message = Message("me","you","something very important")
  }

  //now same thing with default parameters
  case class MessageWithDefault(
    val sender: String = "me",
    val receiver: String = "you",
    val body: String = "something very important"
  )

  //compare instances
  myMessage == Message.EmptyMessage

  //interfaces aka mixins aka traits
  trait Identifyable{
    def id:Int
  }

  //define trait
  trait Printable{
    def print():Unit
  }

  //extend a class with Printable
  case class PrintableMessage(val sender:String, val receiver: String,
                              val body: String, val createdAt:Long=System.currentTimeMillis())
    extends Printable with Identifyable{
      //define print method ! string interpolation !
      def print() = println(s"message from $sender to $receiver: $body")

      //implement the id method as a val
      val id = (sender + receiver + body).hashCode

      //define a lazy val just because we can
      lazy val reversedBody = body.reverse
    }

  val myPMEssage = PrintableMessage("me","you","hello")
  //extracted fields
  val PrintableMessage(s,r,b,_) = myPMEssage

  //create a printable via new keyword
  val myPrintable = new Printable {
    def print(): Unit = println("i'm a Printable")
  }

  //we can also provide implementations in traits
  trait PrintingID{
    //restricting to Identifyables!
    self:Identifyable =>
      def print() = println(s"id: ${self.id}")
  }

  //define a method that constructs PrintingID objects
  //note: show without extending identifyable and why it doesn't work
  def PrintingID(pid:Int):PrintingID = new Identifyable with PrintingID{
    lazy val id = pid
  }


}
