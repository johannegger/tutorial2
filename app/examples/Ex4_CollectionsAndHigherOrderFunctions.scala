package examples

object Ex4_CollectionsAndHigherOrderFunctions {
  import Ex1_FunctionsAndVariables._

  //collections
  val myList = List("apple","pear","banana","cherry","apple")
  //distinct
  val mySet = myList.toSet

  //ranges
  val myRange = (0 to 9) //same as (0 until 10 by 1)

  //special type Option. not really a collection but has a lot of methods in common
  val maybeCherry = Option("Cherry")
  None
  Some("")

  //map!!
  val myReversedUpperSet = mySet.map{ fruit => reversedAndUpper(fruit) }
  //or
  mySet map reversedAndUpper

  //filter
  val filtered = mySet.filter( fruit => fruit.contains("e") )

  //find --> option type!!
  val maybePear = mySet.find(_ == "pear")

  //filter + map => collect!
  mySet.collect{ case fruit if fruit.contains("e") => reversedAndUpper(fruit) }

  //find and map => collectFirst!
  val pickedAndUpperCased = mySet.collectFirst{ case f @ "cherry" => uppercased(f) }

  //zippedwithIndex -> a better version of for loop with index
  val withIndex = myList.zipWithIndex
  withIndex.map{ case (index,fruit) => println(s"$index : $fruit")}

  //folding... and creating a Map with counts
  val fruitsWithCount = myList.foldLeft(Map.empty[String,Int])(
    (curMap,fruit) =>
      curMap + (fruit-> (curMap.getOrElse(fruit,0) + 1) )
  )

  //sorting
  //with implicit ordering -> scala brings it for primitive types
  val alphaSorted = mySet.toList.sorted

  //other important collection operations: exists, headOption, diff, intersect, takeWhile, dropWhile, partition, sort, unzip


}
