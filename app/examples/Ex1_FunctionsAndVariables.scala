package examples

object Ex1_FunctionsAndVariables{

  //defining variables
  val a = "a string"
  val b:Long  = 1000

  //defining functions
  //1. by def
  def reversed(input:String):String = {
    input.reverse
  }

  def twoParams(in1:Double,in2:Int) = math.round(in1) + in2

  //calling function with named parameters
  twoParams(in2=99,in1=3.5)

  //2. as a variable
  val uppercased = (in:String) => in.toUpperCase()

  // or shorter with underscore as an anonymous function
  (_:String).toUpperCase()

  //3. implementing functionX
  val uppercased2 = new Function1[String,String] {
    def apply(in:String) = in.toUpperCase
  }

  //function composition!
  def reversedAndUpperWithParam(in:String):String = uppercased(reversed(in))
  //same as:
  def reversedAndUpper = uppercased andThen reversed

  //lazy values and call-by-name aka lazy parameter evaluation
  lazy val content = {
    println("evaluating content")
    "this is the content of a file and it's only read when we first access it"
  }
  //call-by-name, still not evaluating anything
  def printContents(content: => String) =
    if (scala.util.Random.nextInt % 4 == 0) println(content)
    else println("didn't evaluate the content")

  //printContents(content)

}
