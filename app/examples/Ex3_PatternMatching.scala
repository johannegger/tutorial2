package examples

object Ex3_PatternMatching {
  import org.joda.time.DateTime
  import Ex2_ClassesObjectsAndTraits._

  case object MySingleton
  val myVal = "hello"
  def twoDaysAgoTs = new DateTime().minusDays(2).getMillis

  //pattern matching
  def stringFor(in:Any):String = in match{
    case MySingleton => "it's the singleton"
    case `myVal` => s"it's $myVal"
    //case class extractor and assignment and guard
    case pm @ PrintableMessage(sender,receiver,body,created) if body.length > 5 && created > twoDaysAgoTs =>
      s"it's a PrintableMessage, its body has more than 5 characters and its reversed field is ${pm.reversedBody}"
    case pm @ PrintableMessage(_,_,_,_) => s"PrintableMessage with id ${pm.id}"
    case idObj : Identifyable => s"id: ${idObj.id}"
    case _ => "unknown"
  }

}
