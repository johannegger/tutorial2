package controllers

import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.libs.ws.WS
import play.api.mvc._
import play.api.Play.current
import scala.concurrent.ExecutionContext.Implicits.global
import models.{ActorDB => DB}
/*
*
* a simple message storage system
* endpoint to store an incoming message
* endpoint to fetch a message by id
* json-converters for type message
* simple thread safe storage implemented as an actor
*
* */

object Application extends Controller {

  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  //store any kind of json-object
  //using a body parser of type json
  def stored = Action.async(parse.json[JsObject]){ req =>
    //calling DB.persist results in an future of option of JsObject
    DB.persist(req.body).
    map{ case result => //access the result via map
      result.map(Ok(_)).  //if the result was of type Some we map the Ok status
      getOrElse(InternalServerError) //else we return a server error
    }
  }

  //getter
  def byID(id:Int) = Action.async{ implicit req =>
    DB.getByKey(id).map{ case mResult =>
      mResult.map{ case result => Ok(result) }.
      getOrElse( NotFound(
        Json.obj(
          "error" -> s"no object with id: $id was found"
        )
      ))
    }
  }

  //-------- JSON ---------
  import examples.Ex2_ClassesObjectsAndTraits._

  //creating a reader and a writer for message -> Format
  private lazy val explicitFormat = (
    (__ \ "from").format[String] and  //functional combinators!
    (__ \ "to"  ).format[String] and
    (__ \ "body").format[String]
  )(Message.apply,unlift(Message.unapply))

  //another way of defining a format for case classes (Caution: it's NOT an OFormat !)
  private implicit lazy val macroFormat = Json.format[Message]

  def storedMessage = Action.async(parse.json[Message]){ implicit req =>
    //and back to json to store it
    val jMessage = Json.toJson(req.body)
    //same as: oFormat.writes(req.body)

    DB.persist(jMessage).map{ case result =>
      result.map(Ok(_)).getOrElse(InternalServerError)
    }
  }

  def all = Action.async{ implicit req =>
    DB.dump.map( msgs => Ok(Json.toJson(msgs)))
  }

  //---- Webservice requests!
  def wsRequest = Action.async{

    val res = WS.url("http://localhost:9000/all").get().map{ case result =>
      result.json.validate[List[Message]].fold(
        error => {
          s"an error occured $error ${result.body}"
        },
        _.map(_.body).mkString(sys.props("line.separator"))
      )
    }
    res.map{ case answer => Ok(Json.toJson(answer))}

  }


}

